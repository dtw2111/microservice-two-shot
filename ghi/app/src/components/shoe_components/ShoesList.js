import React from "react";
import { Link } from "react-router-dom";
// import ShoeForm from "./ShoeForm";



class ShoesList extends React.Component {
    constructor() {
        super();
        this.state = {
            shoes: [],
        }
    }

    async componentDidMount() {
        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const response = await fetch(shoeUrl);
        if (response.ok) {
            const data = await response.json();
            this.setState({shoes: data.shoes})
        }
    }

    async handleDelete(event) {
        const href = event.target.value;
        const url = `http://localhost:8080${href}`;
        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            this.setState({shoes: this.state.shoes.filter(shoe => shoe.href !== href)})
            console.log(`shoe at href ${href} successfully deleted`);
        } else {
            throw new Error("Response not ok");
        }
    }

    render () {
        return (
        <div className="my-5 container">
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                <Link to="new">
                    <button className="btn btn-outline-primary">Create new shoe</button>
                </Link>
            </div>
            <table className="table my-5 table-striped">
                <thead>
                    <tr>
                        <th></th>
                        <th>Manufacturer</th>
                        <th>Model name</th>
                        <th>Color</th>
                        <th>Bin closet name</th>
                    </tr>
                </thead>
                <tbody>
                {this.state.shoes.map((shoe) => {
                    return (
                        <tr key={shoe.href}>
                            <td>
                                <img className="bg-white rounded shadow d-block mx-auto" src={shoe.picture_url} width="200" />
                            </td>
                            <td>{shoe.manufacturer}</td>
                            <td>{shoe.model_name}</td>
                            <td>{shoe.color}</td>
                            <td>{shoe.bin}</td>
                            <td>
                                <button className='btn btn-outline-danger' value={shoe.href} onClick={this.handleDelete.bind(this)}>Delete</button>
                            </td>
                        </tr>
                    )
                })}
                </tbody>
            </table>
        </div>
        ) 
    }
}

export default ShoesList;
import React from "react";

class ShoeForm extends React.Component {
  constructor() {
    super();
    this.state = {
      model_name: "",
      manufacturer: "",
      color: "",
      picture_url: "",
      bins: [],
    };
  }

  async componentDidMount() {
    const binsUrl = "http://localhost:8100/api/bins/";
    const response = await fetch(binsUrl);

    if (response.ok) {
      const data = await response.json();
      this.setState({ bins: data.bins });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    delete data.bins;

    const url = `http://localhost:8080/api/bins/${data.bin}/shoes/`;
    delete data.bin;

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      const newShoe = await response.json();
      console.log("newShoe: ", newShoe);
      this.setState({
        model_name: "",
        manufacturer: "",
        color: "",
        picture_url: "",
        bin: "",
      });
    }
  }

  handleChangeModelName(event) {
    const value = event.target.value;
    this.setState({ model_name: value });
  }

  handleChangeManufacturer(event) {
    const value = event.target.value;
    this.setState({ manufacturer: value });
  }

  handleChangeColor(event) {
    const value = event.target.value;
    this.setState({ color: value });
  }

  handleChangePictureUrl(event) {
    const value = event.target.value;
    this.setState({ picture_url: value });
  }

  handleChangeBin(event) {
    const value = event.target.value;
    this.setState({ bin: value });
  }

  render() {
    return (
      <div className="container mb-4">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>It's shoe time!</h1>
              <form
                onSubmit={this.handleSubmit.bind(this)}
                id="create-conference-form"
              >
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleChangeModelName.bind(this)}
                    value={this.state.model_name}
                    placeholder="ModelName"
                    required
                    type="text"
                    name="model_name"
                    id="model_name"
                    className="form-control"
                  />
                  <label htmlFor="model_name">Model name</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleChangeManufacturer.bind(this)}
                    value={this.state.manufacturer}
                    placeholder="Manufacturer"
                    required
                    type="text"
                    name="manufacturer"
                    id="manufacturer"
                    className="form-control"
                  />
                  <label htmlFor="manufacturer">Manufacturer</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleChangeColor.bind(this)}
                    value={this.state.color}
                    placeholder="Color"
                    required
                    type="text"
                    name="color"
                    id="color"
                    className="form-control"
                  />
                  <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleChangePictureUrl.bind(this)}
                    value={this.state.picture_url}
                    required
                    placeholder="PictureUrl"
                    type="text"
                    name="picture_url"
                    id="picture_url"
                    className="form-control"
                  />
                  <label htmlFor="picture_url">Picture URL</label>
                </div>
                <div className="mb-3">
                  <select
                    onChange={this.handleChangeBin.bind(this)}
                    value={this.state.bin}
                    required
                    placeholder="Bin"
                    name="bin"
                    id="bin"
                    className="form-select"
                  >
                    <option value="">Choose a bin</option>
                    {this.state.bins.map((bin) => {
                      return (
                        <option key={bin.id} value={bin.id}>
                          {bin.closet_name}
                        </option>
                      );
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ShoeForm;

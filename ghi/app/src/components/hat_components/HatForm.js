import React from "react";

class HatForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      style_name: "",
      fabric: "",
      color: "",
      picture: "",
      locations: [],
    };
  }

  async componentDidMount() {
    const locationUrl = "http://localhost:8100/api/locations/";

    const response = await fetch(locationUrl);

    if (response.ok) {
      const data = await response.json();
      this.setState({ locations: data.locations });
    }
  }

  async submitHandler(e) {
    e.preventDefault();
    const data = { ...this.state };
    delete data.locations;

    const locationUrl = `http://localhost:8090/api/locations/${data.location}/hats/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newHat = await response.json();
      this.props.updateHatsList(newHat);
    }
  }

  inputChangeHandler(e) {
    const value = e.target.value;
    this.setState({ [e.target.name]: value });
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a Hat</h1>
            <form
              onSubmit={this.submitHandler.bind(this)}
              id="create-conference-form"
            >
              <div className="form-floating mb-3">
                <input
                  required
                  placeholder="Style Name"
                  type="text"
                  name="style_name"
                  id="name"
                  className="form-control"
                  onChange={this.inputChangeHandler.bind(this)}
                />
                <label htmlFor="name">Style Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  required
                  placeholder="Fabric"
                  type="text"
                  name="fabric"
                  id="fabric"
                  className="form-control"
                  onChange={this.inputChangeHandler.bind(this)}
                />
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  required
                  placeholder="Color"
                  type="text"
                  name="color"
                  id="color"
                  className="form-control"
                  onChange={this.inputChangeHandler.bind(this)}
                />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  required
                  placeholder="Picture"
                  type="text"
                  name="picture"
                  id="picture"
                  className="form-control"
                  onChange={this.inputChangeHandler.bind(this)}
                />
                <label htmlFor="picture">Picture</label>
              </div>
              <div className="mb-3">
                <select
                  required
                  name="location"
                  id="location"
                  className="form-select"
                  onChange={this.inputChangeHandler.bind(this)}
                >
                  <option value="">Choose a location</option>

                  {this.state.locations.map((location) => {
                    return (
                      <option key={location.id} value={location.id}>
                        {location.closet_name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default HatForm;

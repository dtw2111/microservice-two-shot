import React from "react";
import HatForm from "./HatForm";

class HatList extends React.Component {
  constructor() {
    super();
    this.state = {
      hats: [],
    };
  }

  async componentDidMount() {
    const url = "http://localhost:8090/api/hats/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ hats: data.hats });
    }
  }

  async deleteHat(e) {
    const url = `http://localhost:8090${e.target.value}`;
    const fetchConfig = {
      method: "delete",
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);
    console.log(response);

    if (response.ok) {
      this.setState({
        hats: this.state.hats.filter((hat) => hat.href !== e.target.value),
      });
    }
  }

  updateHatsList(newHat) {
    console.log(this.state);
    this.setState({ hats: this.state.hats.push(newHat) });
  }

  render() {
    return (
      <React.Fragment>
        <HatForm updateHatsList={this.updateHatsList} />
        <div>Your Hats</div>
        <table className="table table-striped">
          <thead>
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Location</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            {this.state.hats.map((hat) => {
              return (
                <tr key={hat.href}>
                  <td>{hat.style_name}</td>
                  <td>{hat.location}</td>
                  <td>
                    <button
                      value={hat.href}
                      onClick={this.deleteHat.bind(this)}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </React.Fragment>
    );
  }
}

export default HatList;

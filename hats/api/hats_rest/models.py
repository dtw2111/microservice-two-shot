from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
  import_href = models.CharField(max_length=200, unique=True)
  closet_name = models.CharField(max_length=200)


class Hat(models.Model):
  style_name = models.CharField(max_length=200)
  fabric = models.CharField(max_length=200)
  color = models.CharField(max_length=200, null=True)
  picture = models.URLField(max_length=200, null=True)
  location = models.ForeignKey(
    LocationVO,
    related_name="hats",
    on_delete=models.CASCADE,
    null=True,
  )


  def __str__(self):
    return self.name

  def get_api_url(self):
    return reverse("api_show_hat", kwargs={"pk": self.pk})

